﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Dragandtest::OnMouseUp()
extern void Dragandtest_OnMouseUp_m9BAD4FBDCCDB84EFA8CFFB4B8A52BC64FAF61435 (void);
// 0x00000002 System.Void Dragandtest::OnMouseDown()
extern void Dragandtest_OnMouseDown_mBB8E43C38E1C1563FEA34E2521B159BEC2AB5F43 (void);
// 0x00000003 System.Void Dragandtest::Start()
extern void Dragandtest_Start_m205411F392AA6781DFACFBF929C4BA40D64DD185 (void);
// 0x00000004 System.Void Dragandtest::Update()
extern void Dragandtest_Update_m3AAE5E1095D48E32AC30E8BCD02852CD9042318C (void);
// 0x00000005 System.Void Dragandtest::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Dragandtest_OnTriggerEnter2D_m59470886215EBD2151770EDE0BA1136ED80D88B8 (void);
// 0x00000006 System.Void Dragandtest::move_back()
extern void Dragandtest_move_back_m86AC18F543A633B8F494E8289A13F9AE5B9B42FA (void);
// 0x00000007 System.Collections.IEnumerator Dragandtest::smallDelay(System.Single)
extern void Dragandtest_smallDelay_mAA2592D9DE471D91E49D83391AD4DEC86D9593CE (void);
// 0x00000008 System.Void Dragandtest::.ctor()
extern void Dragandtest__ctor_mCFAFE3B6817F15F2597EB37FF000492941EF68C0 (void);
// 0x00000009 System.Void Dragandtest/<smallDelay>d__20::.ctor(System.Int32)
extern void U3CsmallDelayU3Ed__20__ctor_m3B833C935400A24C83FF5A4ADBD1A2311E12618D (void);
// 0x0000000A System.Void Dragandtest/<smallDelay>d__20::System.IDisposable.Dispose()
extern void U3CsmallDelayU3Ed__20_System_IDisposable_Dispose_m8E7C2C24298BCA7435414B6AC88FA2D3302D4A42 (void);
// 0x0000000B System.Boolean Dragandtest/<smallDelay>d__20::MoveNext()
extern void U3CsmallDelayU3Ed__20_MoveNext_m09FBAD51FF6DAA91A42C7A7A9F1AD3A64F7E2E12 (void);
// 0x0000000C System.Object Dragandtest/<smallDelay>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsmallDelayU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5390A2F5865A661F60453C3CBA869EC2A48E0B15 (void);
// 0x0000000D System.Void Dragandtest/<smallDelay>d__20::System.Collections.IEnumerator.Reset()
extern void U3CsmallDelayU3Ed__20_System_Collections_IEnumerator_Reset_m5E0F32A3F20DCDC44969C978F0D65E28C543D644 (void);
// 0x0000000E System.Object Dragandtest/<smallDelay>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CsmallDelayU3Ed__20_System_Collections_IEnumerator_get_Current_m7773B1D7AD609202DD491C44CEF4E12D9C8160A8 (void);
// 0x0000000F System.Void DropItem::Start()
extern void DropItem_Start_m2469629C900EE266B93F8501EDED2FC047ADA2D2 (void);
// 0x00000010 System.Void DropItem::Update()
extern void DropItem_Update_m13E548654B060C77D722622351201FBCEA7DC035 (void);
// 0x00000011 System.Void DropItem::.ctor()
extern void DropItem__ctor_m2BF41DC1C3C9229BACD1F615B0DA68AC7A9DDF52 (void);
// 0x00000012 System.Void GameManager::Start()
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
// 0x00000013 System.Void GameManager::Update()
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
// 0x00000014 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x00000015 System.Void timer::Start()
extern void timer_Start_m7D1C9013C196C3DA2F0DA3777D4F3C3C3A9848AB (void);
// 0x00000016 System.Void timer::Update()
extern void timer_Update_mE4277E5A86DF8A939C7A765DB92E82DC55DF7EF1 (void);
// 0x00000017 System.Void timer::DisplayTime(System.Single)
extern void timer_DisplayTime_m3CAB540A4CEAAFED90DA726B06CDCD9831C4EF9B (void);
// 0x00000018 System.Void timer::.ctor()
extern void timer__ctor_mF81B52E3A68DAF2DE43EA77E1FBBD683FF2F399D (void);
static Il2CppMethodPointer s_methodPointers[24] = 
{
	Dragandtest_OnMouseUp_m9BAD4FBDCCDB84EFA8CFFB4B8A52BC64FAF61435,
	Dragandtest_OnMouseDown_mBB8E43C38E1C1563FEA34E2521B159BEC2AB5F43,
	Dragandtest_Start_m205411F392AA6781DFACFBF929C4BA40D64DD185,
	Dragandtest_Update_m3AAE5E1095D48E32AC30E8BCD02852CD9042318C,
	Dragandtest_OnTriggerEnter2D_m59470886215EBD2151770EDE0BA1136ED80D88B8,
	Dragandtest_move_back_m86AC18F543A633B8F494E8289A13F9AE5B9B42FA,
	Dragandtest_smallDelay_mAA2592D9DE471D91E49D83391AD4DEC86D9593CE,
	Dragandtest__ctor_mCFAFE3B6817F15F2597EB37FF000492941EF68C0,
	U3CsmallDelayU3Ed__20__ctor_m3B833C935400A24C83FF5A4ADBD1A2311E12618D,
	U3CsmallDelayU3Ed__20_System_IDisposable_Dispose_m8E7C2C24298BCA7435414B6AC88FA2D3302D4A42,
	U3CsmallDelayU3Ed__20_MoveNext_m09FBAD51FF6DAA91A42C7A7A9F1AD3A64F7E2E12,
	U3CsmallDelayU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5390A2F5865A661F60453C3CBA869EC2A48E0B15,
	U3CsmallDelayU3Ed__20_System_Collections_IEnumerator_Reset_m5E0F32A3F20DCDC44969C978F0D65E28C543D644,
	U3CsmallDelayU3Ed__20_System_Collections_IEnumerator_get_Current_m7773B1D7AD609202DD491C44CEF4E12D9C8160A8,
	DropItem_Start_m2469629C900EE266B93F8501EDED2FC047ADA2D2,
	DropItem_Update_m13E548654B060C77D722622351201FBCEA7DC035,
	DropItem__ctor_m2BF41DC1C3C9229BACD1F615B0DA68AC7A9DDF52,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	timer_Start_m7D1C9013C196C3DA2F0DA3777D4F3C3C3A9848AB,
	timer_Update_mE4277E5A86DF8A939C7A765DB92E82DC55DF7EF1,
	timer_DisplayTime_m3CAB540A4CEAAFED90DA726B06CDCD9831C4EF9B,
	timer__ctor_mF81B52E3A68DAF2DE43EA77E1FBBD683FF2F399D,
};
static const int32_t s_InvokerIndices[24] = 
{
	3409,
	3409,
	3409,
	3409,
	2784,
	3409,
	2454,
	3409,
	2767,
	3409,
	3258,
	3317,
	3409,
	3317,
	3409,
	3409,
	3409,
	3409,
	3409,
	3409,
	3409,
	3409,
	2815,
	3409,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	24,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
