using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragandtest : MonoBehaviour
{

    public GameObject selectedObject;
    public Vector3 offset;
    public Vector3 mousePosition;
    // Start is called before the first frame update
    public Vector2 originalPosition;
    public GameObject tireCollision;
    public GameObject wheel_axel;
    public GameObject selected_object;
    [SerializeField] public Sprite tire_sprite, bolt_sprite;
    public GameObject axel1, axel2, boltCollision;
    public timer m_timer;
    bool canMove = true;
    
    public void OnMouseUp()
    {
        move_back();
    }

    public void OnMouseDown()
    {
        selectedObject = transform.gameObject;
    }

    void Start()
    {
        originalPosition = transform.position;
    }


    // Credit to Gamedevbeginner.com for baseline code
    // Update is called once per frame
    void Update()
    {
        if (m_timer.timeValue > 0 && canMove)
        {


            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 1;

            if (Input.GetMouseButtonDown(0))
            {
                Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);


                    Debug.Log("Click!");
                    offset = selectedObject.transform.position - mousePosition;
                

            }
            if (selectedObject)
            {
                selectedObject.transform.position = mousePosition + offset;
            }
            if (Input.GetMouseButtonUp(0) && selectedObject)
            {

                move_back();
            }
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Sup muthahecka");

        if (collision.tag == "Axel" && collision.gameObject.GetComponent<DropItem>().done == false)
        {
            
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = tire_sprite;
            move_back();
            
        }
        if (collision.tag == "BoltHole" && collision.gameObject.GetComponent<DropItem>().done == false)

        {

            collision.gameObject.GetComponent<SpriteRenderer>().sprite = bolt_sprite;
            
            move_back();

        }


    }
    
    void move_back()
    {
        //put back where it was originally placed 
        transform.position = originalPosition;
        selectedObject = null;
        StartCoroutine(smallDelay(.1f));
    }
    IEnumerator smallDelay(float f)
    {
        transform.position = originalPosition;
        canMove = false;
        yield return new WaitForSeconds(f);
        canMove = true;
    } 
}