using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{

    public GameObject win_text, boltCollision;
    public GameObject axel1, axel2, bolt1,bolt2,bolt3,bolt4,bolt5,bolt6,bolt7,bolt8,bolt9,bolt10;
    public static bool hasWon, hasLost; 

    // Start is called before the first frame update
    void Start()
    {
        hasWon = false;
        hasLost = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasLost == true)
        {
            Debug.Log("YOU LOST CANT WIN");
        }

        //when all of the wheels are in
        if (axel1.GetComponent<DropItem>().done == true && axel2.GetComponent<DropItem>().done == true && hasLost == false)  
        {
            boltCollision.SetActive(true);
            hasWon = true;
           //set bolt holes active
            win_text.SetActive(true);
        }

        
    }
}
