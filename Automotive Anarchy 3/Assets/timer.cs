using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    public float timeValue = 10;
    public Text timerText;
    public GameObject LoseText, WinText;
    public static bool hasLost, hasWon; 
    // Update is called once per frame
   
    void Start()
    {
        hasLost = false;
    }
    
    void Update()
    {
        if (timeValue > 0)
        {
            timeValue -= Time.deltaTime;

        }
        else
        {
            timeValue = 0; 
        }

        DisplayTime(timeValue);
    }

    void DisplayTime(float timeToDisplay)
    {
        if (timeToDisplay < 0 && GameManager.hasWon == false)
        {
            timeToDisplay = 0;
            LoseText.SetActive(true);
            hasLost = true;
        }
        if (hasLost == true)
        {
            Debug.Log ("YOU LOST");
        }
           

        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timerText.text = string.Format("{0}", seconds);
    }
}
