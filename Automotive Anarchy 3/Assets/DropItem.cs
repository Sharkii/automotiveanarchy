using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    [SerializeField] public Sprite tire_sprite, bolt_sprite;

    public bool done = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<SpriteRenderer>().sprite == tire_sprite)
        {
            done = true;
        }
        else
        {
            done = false;
        }
        
    }

  

}
